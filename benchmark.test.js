// Import.
const { performance } = require('perf_hooks');
const saltedMd5 = require('./index');

// Constants.
const BENCHMARK_ITERATIONS = 1e3;
const BENCHMARK_MAX_TIME = 10;
const BENCHMARK_DATA = {
  data: 'Some data.',
  salt: 'SUPER-S@LT!',
};

/**
 * Benchmark.
 */
describe('Benchmark.', () => {
  test(`Should hash ${BENCHMARK_ITERATIONS} times in ${BENCHMARK_MAX_TIME}ms.`, () => {
    const { data, salt } = BENCHMARK_DATA;
    const startTime = performance.now();
    for (let i = 0; i < BENCHMARK_ITERATIONS; i++) {
      saltedMd5(data, salt);
    }
    const endTime = performance.now();
    const duration = endTime - startTime;
    if (duration > BENCHMARK_MAX_TIME) {
      throw new Error(`Sync benchmark took too long: ${duration}ms.`);
    }
  });
});
