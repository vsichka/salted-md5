module.exports = [
  {
    files: ['**/*.js'],
    languageOptions: {
      parserOptions: {
        ecmaVersion: 'latest', // Enable parsing of modern ECMAScript features
        sourceType: 'module', // Enable ES Modules
      },
      globals: {
        process: 'readonly', // Allow access to Node.js global `process`
        __dirname: 'readonly', // Allow access to Node.js `__dirname`
        module: 'readonly', // Allow access to Node.js `module`
        require: 'readonly', // Allow use of `require` in CommonJS
      },
    },
    rules: {
      // Possible Problems
      'no-console': 'off', // Allow console logs in Node.js
      'no-process-exit': 'error', // Prevent use of `process.exit()`
      'no-unsafe-finally': 'error', // Prevent unsafe use of `finally`
      // Suggestions
      'prefer-const': 'error', // Prefer const where possible
      'no-var': 'error', // Disallow var, use let or const instead
      'arrow-body-style': ['error', 'as-needed'], // Enforce concise arrow functions
      // Layout and Formatting
      semi: ['error', 'always'], // Require semicolons
      indent: ['error', 2], // Enforce 2-space indentation
      quotes: ['error', 'single'], // Enforce single quotes
      'eol-last': ['error', 'always'], // Enforce newline at end of files
      'comma-dangle': ['error', 'always-multiline'], // Enforce consistent comma usage
      'no-process-exit': 'off', // Allow use of `process.exit()`
    },
  },
];
