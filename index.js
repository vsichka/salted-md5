// Import.
const crypto = require('crypto');

// Constants.
const HASH_ALGORITHM_MD5 = 'md5';
const HASH_DIGEST_HEX = 'hex';
const DEFAULT_DATA = '';
const DEFAULT_SALT = '';

/**
 * Hash data by MD5 algorithm.
 * @param {string} data Data to hash.
 * @return {string} Hash string.
 */
function md5(data) {
  return crypto.createHash(HASH_ALGORITHM_MD5).update(data).digest(HASH_DIGEST_HEX);
}

/**
 * Hash data using salt.
 * @param {string} data Data to hash.
 * @param {string} [salt] Salt.
 * Promise returned if this parameter defined as true.
 * @return {string|Promise<string>} Salted MD5 hash.
 */
function hash(data = DEFAULT_DATA, salt = DEFAULT_SALT) {
  // Define salted data.
  const saltedData = data + salt;

  // Return hash in other cases.
  return md5(saltedData);
}

// Export.
module.exports = hash;
