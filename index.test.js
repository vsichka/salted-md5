// Import.
const saltedMd5 = require('./index');

// Constants.
const TEST_DATA_LIST = [
  {
    data: 'Some data.',
    salt: undefined,
    correctResult: '069972bd107688743d20a177a701490f',
  },
  {
    data: 'Some data.',
    salt: '',
    correctResult: '069972bd107688743d20a177a701490f',
  },
  {
    data: 'Some data.',
    salt: 'SUPER-S@LT!',
    correctResult: '1b25469a2e4b157f02dd1fea3b5f1e9a',
  },
  {
    data: undefined,
    salt: 'SUPER-S@LT!',
    correctResult: '8dcf756d53138e51b5b7355b04b8f2ad',
  },
  {
    data: '',
    salt: 'SUPER-S@LT!',
    correctResult: '8dcf756d53138e51b5b7355b04b8f2ad',
  },
];

/**
 * Test cases.
 */
describe('Test cases.', () => {
  test('Should be equal to precalculated results.', () => {
    for (const { data, salt, correctResult } of TEST_DATA_LIST) {
      const saltedHash = saltedMd5(data, salt);
      expect(saltedHash).toBe(correctResult);
    }
  });
});
